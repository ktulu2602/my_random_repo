"""_summary_
"""
import pytest
from .. import calc


class TestClass:
    """_summary_
    """
    def test_add(self) -> None:
        """_summary_
        """
        assert calc.add(1, 2) == 3

    def test_subtract(self) -> None:
        """_summary_
        """
        assert calc.subtract(-5, 2) == -7

    def test_multiply_without_zero(self) -> None:
        """_summary_
        """
        assert calc.multiply(5, 5) == 25

    def test_multiply_with_zero(self) -> None:
        """_summary_
        """
        assert calc.multiply(5, 0) == 0

    def test_divide_to_zero(self) -> None:
        """_summary_
        """
        with pytest.raises(ZeroDivisionError):
            calc.divide(5, 0)
