# GIT Basics

## Create new branch
```
git branch [branch-name]
git checkout [branch-name]
```
Perform changes in the newly created branch, add them and commit them.
Push them to the remote branch using the `git push -u origin [branch-name]` command which syncs the local branch to the remote branch.

`git branch -a` will show you local and remote branches

## Merging branches

Steps for merging `new-branch` in another branch called `dev` - `git branch --merged` will show you already merged branches
```
git checkout dev
git pull origin dev
git merge new-branch
git push origin dev
```

## Delete merged branches
```
git branch --merged (to see all merged branches)
git branch -d new-branch (deletes locally)
git branch -a (to see all branches locally and remote)
git push origin --delete new-branch (deletes remote branch)
```
