"""Test code for git studies"""


def add(num_1: int, num_2: int) -> int:
    """Function adds 2 numbers

    Args:
        num_1 (int): number 1
        num_2 (int): number 2

    Returns:
        int: sum of a and b
    """
    return num_2 + num_1


def subtract(num_1: int, num_2: int) -> int:
    """Function subtracts 2 numbers

    Args:
        num_1 (int): number 1
        num_2 (int): number 2

    Returns:
        int: sum of a and b
    """
    return num_1 - num_2


def divide(num_1: int, num_2: int) -> int:
    """Divide

    Args:
        num_1 (int): _description_
        num_2 (int): _description_

    Returns:
        int: _description_
    """
    return num_1 / num_2


def multiply(num_1: int, num_2: int) -> int:
    """Multiply function

    Args:
        num_1 (int): _description_
        num_2 (int): _description_

    Returns:
        int: _description_
    """
    return num_1 * num_2


def my_square(num_1: int) -> int:
    """Square function

    Args:
        num_1 (int): _description_

    Returns:
        int: _description_
    """
    return num_1 * num_1
